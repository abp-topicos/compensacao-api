from flask import Flask
from dotenv import load_dotenv
import os
from workers.start_consuming import consume_compensation
import threading


def create_app():
    load_dotenv()
    app = Flask(__name__, template_folder='template')
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATABASE_URI')
    consume_compensation()
    # consumer_thread = threading.Thread(target=consume_compensation)
    # consumer_thread.daemon = True
    # consumer_thread.start()

    return app
