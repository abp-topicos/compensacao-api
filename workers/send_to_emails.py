import pika
import json


def send_to_emails(payload, channel):
    try:
        queue_info = channel.queue_declare(queue='email', passive=True)

        if queue_info.method.message_count == 0:
            print("Fila 'email' não encontrada, criando...")
            channel.queue_declare(queue='email', durable=True)

        message_id = 1
        channel.basic_publish(
            exchange='',
            routing_key='email',
            body=json.dumps(payload),
            properties=pika.BasicProperties(
                delivery_mode=2,
                headers={'x-message-id': message_id}
            ),
            mandatory=True
        )
        channel.confirm_delivery()
        print({'status': 'Mensagem enviada para "email"', 'message_id': message_id})
    except Exception as e:
        raise ValueError("Erro: ", e)
