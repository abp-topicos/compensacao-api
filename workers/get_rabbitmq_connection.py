import os
import pika


def rabbitmq_connection():
    rabbitmq_uri = os.getenv('RABBIT_URI')
    try:
        connection = pika.BlockingConnection(
            pika.URLParameters(str(rabbitmq_uri)))
        print(connection.is_open)
        return connection
    except Exception as error:
        return f"Erro ao conectar ao RabbitMQ: {error}"