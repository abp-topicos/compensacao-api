import requests


def send_to_boletos(id, ch, method):
    response = requests.post('http://127.0.0.1:5000/boleto/{}/compensar'.format(id))
    if response.status_code == 201:
        print("Alteração de boletos enviada com sucesso")
    else:
        print(f"Erro ao alterar boletos: {response.status_code}")
        ch.basic_ack(delivery_tag=method.delivery_tag)
