import json
from workers.get_rabbitmq_connection import rabbitmq_connection
from workers.send_to_boletos import send_to_boletos
from workers.send_to_emails import send_to_emails
from time import sleep


def consume_compensation():
    try:
        connection = rabbitmq_connection()
        channel = connection.channel()
        channel.queue_declare(queue='compensacao', passive=True)

        def callback(ch, method, properties, body):
            mensagem = body.decode('utf-8')

            mensagem_json = json.loads(mensagem)
            id = mensagem_json.get('id')

            sleep(5)
            data = {
                "nome": mensagem_json.get('nome_pessoa'),
                "email": mensagem_json.get('email_pessoa'),
                "status": "Compensado"
            }
            send_to_emails(payload=data, channel=channel)
            send_to_boletos(id=id, ch=ch, method=method)

        channel.basic_consume(queue='compensacao',
                              on_message_callback=callback, auto_ack=True)

        print('Aguardando mensagens. Pressione CTRL+C para sair.')

        channel.start_consuming()

    except Exception as e:
        raise ValueError("Error: ", e)
