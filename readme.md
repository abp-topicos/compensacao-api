
# Microsserviço para a alteração da compensação do boleto e envio para a fila de Emails 🚀.

Este projeto utiliza Python como Microsserviço. Para rodá-lo você precisa ter o python instalado no seu computador. Você pode instalá-lo neste link: https://www.python.org/downloads/


## Rodando localmente

Clone o projeto

```bash
    git clone https://gitlab.com/abp-topicos/compensacao-api
```

Entre no diretório do projeto

```bash
    cd compensacao-api
```

Criação do ambiente virtual

```bash
    python -m venv ./venv
```

Ativação do ambiente virtual

```bash
    Windows:
    venv/scripts/activate

    Macos/Linux: 
    source venv/bin/activate
```

Instlação das bibliotecas
```bash
    python -m pip install -r requirements.txt

    Ou

    pip install -r requirements.txt
```

Iniciar a aplicação
```bash
    Windows:
    python app.py

    Macos/Linux: 
    python3 app.py
```
## Stack utilizada


**Microsserviço Feito em:** : Python




## Autores

| [Belone Zorzeto Fraga](https://github.com/belonedf) | [Vitor Vassoler Bonfante](https://github.com/VPente) | [Edvan Ronchi](https://github.com/souzavitorhugo) |
|------------------------------------------------------|---------------------------------------------|--------------------------------------------------------|
| ![Foto de Belone Zorzeto Fraga](https://gitlab.com/uploads/-/system/user/avatar/5585894/avatar.png) | ![Foto de Vitor Vassoler Bonfante](https://gitlab.com/uploads/-/system/user/avatar/3811408/avatar.png?width=800) | ![Foto de Edvan Ronchi](https://secure.gravatar.com/avatar/f38ee16564588b6079a7c2d764f29908ff9a0fb304f906dbcea8fe252776c758?s=200&d=identicon) |
