from server.server import create_app
app = create_app()

if __name__ == "__main__":
    app.run('localhost', 3000, debug=True, load_dotenv=True)
